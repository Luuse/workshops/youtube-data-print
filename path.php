<?php if(isset($_GET["person"])){ ?>
	<?php include "engine/start.php" ?>
	<?php $person = $content->person($_GET['person']) ?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<title>Up Next — <?= $person->name ?></title>
	</head>
	<body>
		<header>
			<h1>Up Next</h1>
			<h2><?= $person->name ?></h2>
		</header>
		<main>
			<?php foreach($person->urls as $url=>$datas): ?>
				<ul class="url">
					<?php foreach($datas as $name=>$variable): ?>
						<?php include "snippets/item.php" ?>
					<?php endforeach ?>
				</ul>
			<?php endforeach ?>	
		</main>
	</body>
	</html>
<?php } ?>
