<?php

class Fetch extends Access{

	protected $dataList;
	protected $dataTypes;
	protected $commentTypes;

	public function __construct($data){

		$this->dataList = $data;
		$this->dataTypes = [
			"snippet" => ["publishedAt", "title", "description", "thumbnails", "tags"],
			"contentDetails" => ["duration"],
			"statistics" => ["viewCount", "likeCount", "dislikeCount", "favoriteCount", "commentCount"]
		];

	}

	public function getVideoData($id, $endpoint){

		$headers = [

			'Accept: application/json'
		];

		$ch = curl_init($this->query($id, $endpoint));

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);


		$data = curl_exec($ch);
		curl_close($ch);

		if(!isset(json_decode($data)->error)){

			return json_decode($data);

		}else{

			throw new Exception($data);
		}

	}

	public function getPersons(){

		$files = glob("content/url/*.json");
		$persons = [];

		if(count($files) > 0){

			foreach($files as $file){

				$datas = $this->loadJSON($file);

				foreach ($datas as $name => $urls) {

					$person = new StdClass;

					$person->id = $this->slugify($name);
					$person->file = pathinfo($file)["filename"];
					$person->name = $name;
					$person->urls = $urls;

					$persons [] = $person;

				}
			}

			return $persons;

		}else{

			echo "No json files in content/url.<br>";
			die();
		}

	}

	public function fillItem($item, $name, $value, $type){

		if(isset($value) && in_array($name, $this->dataList[$type])){

			$item[$name] = $value;
		}

		return $item;
	}

	public function fetchData($person){

		$dir = "content/output/".$person->file;
		$output = $dir."/data.json";

		$this->createDir($dir);

		if(!file_exists($output)){

			foreach($person->urls as $key=>$url){

				$id = $this->getId($url);
				$item = [];

				try{

					$datas = $this->getVideoData($id, "/videos?part=snippet,contentDetails,statistics,recordingDetails&id=".$id)->items[0];

					$item = $this->fillItem($item, "url", $url, "video");
					unset($person->urls[$key]);

					foreach($this->dataTypes as $k=>$type){

						foreach($type as $name){

							if($name !== "thumbnails"){ 

								if(property_exists($datas->$k, $name)){

									$item=$this->fillItem($item, $name, $datas->$k->$name, "video");
								}

							}else{

								if(property_exists($datas->$k->$name, "standard")){

									$item=$this->fillItem($item, $name, $datas->$k->$name->standard->url, "video");
								}
							}
						}
					}

					$comments = $this->getVideoData($id, "/commentThreads?part=snippet&maxResults=10&videoId=".$id);

					if(!empty($comments) && isset($comments->items) && isset($this->dataList["comments"])){

						$item["comments"] = [];

						foreach($comments->items as $part){

							$comment = [];
							$snip = $part->snippet->topLevelComment->snippet;

							foreach($this->dataList["comments"] as $label){

								$comment[$label] = $snip->$label;
							}

							$item["comments"] [] = $comment;
						}

					}

					$person->urls[$url] = $item;

				}catch(Exception $e){

					echo 'Caught exception: ',  $e->getMessage(), "\n";

				}

				file_put_contents($output, json_encode($person));

			}

			echo $output." file created.<br>";
		}
	}

	public function html($person){

		$output = "content/output/".$person->file."/index.html";

		if(!file_exists($output)){

			$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			$url = $actual_link."path.php?person=".$person->id;
			$html = file_get_contents($url);
			file_put_contents($output, $html);

			echo $output." file created.<br>";
		}
	}

	public function writeDatas(){

		$persons = $this->getPersons();

		foreach ($persons as $person){

			$this->fetchData($person);
			$this->html($person);
		}
	}

}