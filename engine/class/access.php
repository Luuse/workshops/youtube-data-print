<?php

class Access extends Data{

	protected static $_url;
	protected static $_key;

	public function __construct($key){

			self::$_url = "https://www.googleapis.com/youtube/v3";
			self::$_key = $key;

		if($key == ""){

			echo "Fill in YouTube Data Api key in config/config.php.<br>";
		}
	}

	protected function endPoint($id, $endpoint){

		return $endpoint."&key=".self::$_key;
	}

	protected function query($id, $endpoint){

		return self::$_url.$this->endPoint($id, $endpoint);
	}
}