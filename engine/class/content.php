<?php

class Content extends Data{

	public function persons(){

		$folders = glob("content/output/*", GLOB_ONLYDIR);
		$persons = [];

		foreach ($folders as $folder) {
			
			$persons [$this->loadJSON($folder."/data.json")->id] = $this->loadJSON($folder."/data.json");
		}

		return $persons;
	}

	public function person($id){

		if(isset($this->persons()[$id])){

			return $this->persons()[$id];

		}else{

			echo "ERROR: This Person Doesn't Exist.";
			die();
		}
	}

}