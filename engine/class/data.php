<?php

class Data extends Strings{

	public function loadJSON($path){

		return json_decode(file_get_contents($path));
	}

	public function createDir($path){

		if(!file_exists($path)){

			mkdir($path);
		}

	}
}