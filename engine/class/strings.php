<?php

class Strings{

	public function slugify($text){

		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		$text = trim($text, '-');
		$text = preg_replace('~-+~', '-', $text);
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}

	public function getId($ytUrl){

		return substr(explode("?v=", $ytUrl)[1], 0, 11);
	}

	public function toTime($date){

		return str_replace("M", ".", substr($date, 2, strlen($date)-3))." min";
	}
}