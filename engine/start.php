<?php

require_once 'config/config.php';

include 'class/strings.php';
include 'class/data.php';
include 'class/access.php';
include 'class/fetch.php';
include 'class/content.php';

$access = new Access($key);
$fetch = new Fetch($data);
$content = new Content();
$strings = new Strings();

$key="";