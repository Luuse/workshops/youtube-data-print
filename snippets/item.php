
<?php if($name == "title"): ?>
<h2><?= $variable ?></h2>
<?php else: ?>
<div class="<?= $name ?>">
	<span class="name"><?= $name ?></span>
	<?php if($name !== "thumbnails" && $name !== "comments"): ?>
		<?php if(!is_array($variable)): ?>
			<span class="content"><?= ($name == "publishedAt") ? date("d/m/y H:i", strtotime($variable)) : ($name == "duration") ? $strings->toTime($variable) : $variable ?></span>
		<?php else: ?>
			<ul>
				<?php foreach($variable as $item): ?>	
					<li><?= $item ?></li>
				<?php endforeach ?>		
			</ul>
		<?php endif ?>
	<?php elseif($name == "thumbnails"): ?>
		<img src="<?= $variable ?>">
	<?php else: ?>
		<div>
			<?php foreach($variable as $key=>$comment): ?>	
				<ul>
					<?php foreach($comment as $label=>$content): ?>
					<li class="<?= $label ?>">
						<?php if($label == "publishedAt" || $label == "updatedAt"): ?>
							<?= date("d/m/y H:i", strtotime($content)) ?>
						<?php else: ?>	
							<span class="name"><?= $label ?></span><span class="content">
								<?php if($label == "authorProfileImageUrl"): ?>
									<img src="<?= $content ?>"/>
								<?php else: ?>
									<?= $content ?>
								<?php endif ?>
							</span>
						<?php endif ?>	
					</li>
					<?php endforeach ?>
				</ul>
			<?php endforeach ?>		
		</div>
		<?php endif ?>
</div>
<?php endif ?>