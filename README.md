# YouTube Data Print

A small php script to retrieve metadata from a list of YouTube urls, and generate html pages based upon them.

Based on [YouTube Data Api](https://developers.google.com/youtube/v3/).

## Context

This script was built in the context of a workshop by Luuse at ERG (École de Recherche Graphique, Brussels) from 20.01.28 to 20.01.30, called *Up Next*. This workshop aimed at:
1. Studying the behaviour of YT’s recommandation algorithm with the students.
2. Make them design a web-to-print poster out of the metadata.

## Config 

1. First, you have to fill in a personnal YouTube Data Api key in the **config/config.php** file.
2. In the **$data** variable are listed various video metadatas retrieved by the script. Feel free to remove some by commenting them.

## Use

1. Create an empty **your-name.json** file, an save it in **content/url**.
2. Based on the model content/url/lionel.json, fill in the file with your name + an empty list for video urls.

    {"Your Name":[] }

3. Go on YouTube, play a video.
4. Wait until it's over or click on the "Up Next" recommandation.
5. Write down the url of the recommended video in your previously created json file, like this;

    {"Your Name":["https://youtube.com/dkdkqsdqks", "https://youtube.com/dkdkdsdezzqks"] }

6. Repeat this operation until you have a long enough path.
7. Once you're finished, open the index page in a browser.
8. An **index.html** file containing all metadatas related to your recommandation path has been created in **content/ouput/your-name**. You can access it through the index page.
