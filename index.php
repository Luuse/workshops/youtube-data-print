<?php include "engine/start.php" ?>
<?php $fetch->writeDatas() ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Up Next</title>
</head>
<body>
	<header>
		<h1>Up Next</h1>
	</header>
	<main>
		<ul>
			<?php foreach($content->persons() as $key=>$person): ?>
				<li>
					<a href="content/output/<?= $person->file ?>/index.html"><?= $person->name ?></a>
				</li>
			<?php endforeach ?>
		</ul>
	</main>
</body>
</html>
