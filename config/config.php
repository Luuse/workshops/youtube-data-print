<?php

/* KEY: Insert here your personal YouTube Data Api key  */

$key="";


/* 

LIST OF DISPLAYED DATAS:

- Here you can modify which datas should be displayed on html page 
- Comment the line to remove it

*/

$data = [

	"video" => [
		"url",
		"publishedAt",
		"title",
		"description",
		"thumbnails",
		"tags",
		"duration",
		"viewCount",
		"likeCount",
		"dislikeCount",
		"favoriteCount",
		"commentCount"
	],

	"comments" => [
		"publishedAt",
		"updatedAt",
		"authorProfileImageUrl",
		"authorDisplayName",
		"textOriginal",
		"likeCount"
	]

];